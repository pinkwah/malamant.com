module NavHelper
  def nav_link(which, path: nil)
    text = which.capitalize
    path = "/#{which}/" unless path

    if @item.path == path
      "<img src=\"/images/#{which}_button3.gif\" alt=\"#{text}\" title=\"#{text}\" />"
    else
      "<a href=\"#{path}\"><img class=\"nav-img\" src=\"/images/#{which}_button1.gif\" alt=\"#{text}\" title=\"#{text}\" /></a>"
    end
  end
end
